#**Commands reference**

##Mac OS X Mavericks Commands

----------
###Mac OS X  - General
| # |Command|Purpose|
|-|-|-|
|01|uname |Print operating system name, ```man uname```|
|02|ln -s {source} {destination}|Create a symbolic link to a directory|

###Apache web server
| # |Command|Purpose|
|-|-|-|
|01|sudo apachectl start|Start apache server|
|02|sudo apachectl stop|Stop apache server|
|03|sudo apachectl restart|Restart apache server|
|04|sudo apachectl -t|Verify httpd.conf configuration |


----------


----------


##Ubuntu Commands

###Ubuntu  - General
| # |Command|Purpose|
|-|-|-|
|01|uname |Print operating system name, ```man uname```|
|02|ln -s {source} {destination}|Create a symbolic link to a directory|

###Apache web server
| # |Command|Purpose|
|-|-|-|
|01|sudo service apache2 start|Start apache server|
|02|sudo service apache2 stop|Stop apache server|
|03|sudo service apache2 restart|Restart apache server|


----------


----------


##Git Commands
```
$ git 
```


